#include "sensor_util.h"
#include "hal_LCD.h"
#include "main.h"

void display_temp(uint16_t adc_raw_reading, uint16_t divider, ZONE zone) {
    clearLCD();
    // display zone
    if (zone == A) showChar('A', pos1);
    else showChar('B', pos1);

    uint16_t voltage = (long)adc_raw_reading * 1500 / 1024 * divider; // in mV
    int degree = (int)voltage - 500; // degree is real temperature * 10, so we can display a decimal here
    if (degree < 0) {
        degree *= -1;
        // display negative sign
        if (degree >= 1000) LCDMEMW[pos1 / 2] = 0x1;
        else if (degree >= 100) LCDMEMW[pos2 / 2] = 0x1;
        else LCDMEMW[pos3 / 2] = 0x1;
    }

    int integer = 0;
    if (degree >= 1000) {
        showChar((degree / 1000) % 10 + '0', pos2);
        integer = 1;
    }
    if (degree >= 100) {
        showChar((degree / 100) % 10 + '0', pos3);
        integer = 1;
    }
    if (degree >= 10) {
        showChar((degree / 10) % 10 + '0', pos4);
        integer = 1;
    }

    if (integer == 0) {
        showChar('0', pos4);
    }

    // Decimal point
    LCDMEM[pos4+1] |= 0x01;

    showChar((degree / 1) % 10 + '0', pos5);

    // Celsius Unit
    showChar('C', pos6);
    LCDMEM[pos5+1] |= 0x04;
}


/*
 * Note: Moisture sensor output voltage when Vcc is 3.3V and it is completely immersed in water: 2.0V
 */
void display_mois(uint16_t adc_raw_reading, uint16_t divider, ZONE zone) {
    clearLCD();
    // display zone
    if (zone == A) showChar('A', pos1);
    else showChar('B', pos1);

    uint16_t voltage = (long)adc_raw_reading * 1500 / 1024 * divider; // in mV
    uint16_t moisture_percentage = voltage / 2; // moisture_percentage is real percentage * 10

    int integer = 0;
    if (moisture_percentage >= 1000) {
        moisture_percentage = 1000;
        showChar((moisture_percentage / 1000) % 10 + '0', pos2);
        integer = 1;
    }
    if (moisture_percentage >= 100) {
       integer = 1;
       showChar((moisture_percentage / 100) % 10 + '0', pos3);
    }
    if (moisture_percentage >= 10) {
       integer = 1;
       showChar((moisture_percentage / 10) % 10 + '0', pos4);
    }

    if (integer == 0) {
       showChar('0', pos4);
    }

    // Decimal point
    LCDMEM[pos4+1] |= 0x01;

   showChar((moisture_percentage / 1) % 10 + '0', pos5);

    // display percentage %
    LCDMEMW[pos6 / 2] = (0xAA << 8)| (0x27);
}


void display_volt(uint16_t adc_raw_reading, uint16_t divider, ZONE zone) {
   clearLCD();
   // display zone
   if (zone == A) showChar('A', pos1);
   else showChar('B', pos1);

   uint16_t voltage = (long)adc_raw_reading * 1500 / 1024 * divider / 100; // voltage is real voltage * 10, in V

   int integer = 0;
   if (voltage >= 100) {
       integer = 1;
       showChar((voltage / 100) % 10 + '0', pos3);
   }
   if (voltage >= 10) {
       integer = 1;
       showChar((voltage / 10) % 10 + '0', pos4);
   }

   if (integer == 0) {
       showChar('0', pos4);
   }

   // Decimal point
   LCDMEM[pos4+1] |= 0x01;

   showChar((voltage / 1) % 10 + '0', pos5);

   // Volt Unit
   showChar('V', pos6);
}
