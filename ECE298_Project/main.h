#ifndef MAIN_H_
#define MAIN_H_

#include "driverlib/driverlib.h"


#define ADC_HISTORY_TIMER_CYCLE     80  // change this value to configure how long ADC reading history is hold, 80 is around 5 seconds

typedef enum ZONE
{
    A,
    B
} ZONE;

typedef enum READING_TYPE
{
    TEMP,
    MOIS
} READING_TYPE;


#define TIMER_A_PERIOD  1000 //T = 1/f = (TIMER_A_PERIOD * 1 us)
#define HIGH_COUNT      500  //Number of cycles signal is high (Duty Cycle = HIGH_COUNT / TIMER_A_PERIOD)

#define TEMP_A_CHANNEL  ADC_INPUT_A9
#define TEMP_B_CHANNEL  ADC_INPUT_A8
#define MOIS_A_CHANNEL  ADC_INPUT_A7
#define MOIS_B_CHANNEL  ADC_INPUT_A6

//Output pin to buzzer
#define PWM_PORT        GPIO_PORT_P1
#define PWM_PIN         GPIO_PIN7
//LaunchPad LED1 - note unavailable if UART is used
#define LED1_PORT       GPIO_PORT_P1
#define LED1_PIN        GPIO_PIN0
//LaunchPad LED2
#define LED2_PORT       GPIO_PORT_P4
#define LED2_PIN        GPIO_PIN0
//LaunchPad Pushbutton Switch 1
#define SW1_PORT        GPIO_PORT_P1
#define SW1_PIN         GPIO_PIN2
//LaunchPad Pushbutton Switch 2
#define SW2_PORT        GPIO_PORT_P2
#define SW2_PIN         GPIO_PIN6
//Input to ADC - in this case input A9 maps to pin P8.1
#define ADC_IN_PORT     GPIO_PORT_P8
#define ADC_IN_PIN      GPIO_PIN1
#define ADC_IN_CHANNEL  ADC_INPUT_A9

void Init_GPIO(void);
void Init_Clock(void);
void Init_UART(void);
void Init_PWM(void);
void Init_ADC(void);

Timer_A_outputPWMParam param; //Timer configuration data structure for PWM

#endif /* MAIN_H_ */
