#ifndef SENSOR_UTIL_H_
#define SENSOR_UTIL_H_

#include "stdint.h"
#include "main.h"
void display_temp(uint16_t adc_raw_reading, uint16_t divider, ZONE zone);
void display_mois(uint16_t adc_raw_reading, uint16_t divider, ZONE zone);
void display_volt(uint16_t adc_raw_reading, uint16_t divider, ZONE zone);

#endif /* TEMP_SENSOR_H_ */
