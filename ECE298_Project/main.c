#include <sensor_util.h>
#include "main.h"
#include "driverlib/driverlib.h"
#include "hal_LCD.h"
#include <stdio.h>
#include <string.h>
/*
 * -----------------------------------------------------------
 * VARIABLES AND FUNCTIONS TO INTERFACE WITH USER INPUT (UART)
 * -----------------------------------------------------------
 */
#define UART_BUFF_SIZE 256

uint8_t motors_enable[2]; // use function enable_motors() to modify the value
uint16_t thresholds[4]; // use function set_threshold() to modify the value

uint8_t uart_arr[UART_BUFF_SIZE] = { 0 };
uint8_t command[UART_BUFF_SIZE] = { 0 };
uint32_t uart_arr_idx = 0;
bool command_ready = false;
uint16_t command_len = 0;

/*
 * @brief the function is used to set the threshold value in the monitoring system
 * @param value: expected threshold value
 * @param zone: zone to be monitored (A or B)
 * @oaram reading_type: type to be monitored (TEMP or MOIS)
 */
int set_threshold(int value, ZONE zone, READING_TYPE reading_type) {
    value = value * 10;
    uint16_t adc_raw_reading;
    if (reading_type == TEMP) {
        if (value > 1000 || value < -500) return -1;
        int voltage = value + 500; // voltage in mV
        adc_raw_reading = (uint16_t)((uint32_t)voltage * 1024 / 1500);
        if (zone == A) thresholds[3] = adc_raw_reading;
        else thresholds[2] = adc_raw_reading;
    }
    else {
        if (value < 0 || value > 1000) return -1;
        int voltage = value * 2 / 4; // moisture sensor has a divider of 4
        adc_raw_reading = (uint16_t)((uint32_t)voltage * 1024 / 1500);
        if (zone == A) thresholds[1] = adc_raw_reading;
        else thresholds[0] = adc_raw_reading;
    }
    return 0;
}

void enable_motors(ZONE zone) {
    switch(zone) {
    case A:
        motors_enable[0] = 1;
        break;
    case B:
        motors_enable[1] = 1;
        break;
    }
}

void turn_off_motor(int index);

void disable_motors(ZONE zone) {
    switch(zone) {
    case A:
        motors_enable[0] = 0;
        turn_off_motor(1);
        turn_off_motor(3);
        break;
    case B:
        motors_enable[1] = 0;
        turn_off_motor(0);
        turn_off_motor(2);
        break;
    }
}

bool StartsWith(const char *a, const char *b)
{
   if(strncmp(a, b, strlen(b)) == 0) return 1;
   return 0;
}

/*
 * -----------------------------------------------------------
 * End here
 * -----------------------------------------------------------
 */



char ADCState; //Busy state of the ADC
uint16_t ADCResult;
uint8_t ADC_input_channel;
uint16_t ADC_readings[4];
uint8_t motors_ON[4];
/*
 * The following three variables are used to determine whether the ADC reading has been stable above/below a threshold value.
 * If a ADC reading is above/below a threshold value, the counter will be incremented.
 * If the counter value is higher than barrier value, then the reading is considered to be stable above/below a threshold value.
 * The counter value is reset by the history timer. ADC_history_timer is incremented whenever timerA1 interrupts, if its value is above macro ADC_HISTORY_TIMER_CYCLE, it is reset.
 * For now, every 5 seconds the counter value is reset, this means the longest latency for the system is 5 seconds to respond to a threshold triggered event.
 */
uint16_t counters[4];
const uint16_t barrier = 10;
uint16_t ADC_history_timer;

ZONE display_zone;
READING_TYPE display_reading;


// Backup Memory variables to track states through LPM3.5
volatile unsigned char * S1buttonDebounce = &BAKMEM2_L;       // S1 button debounce flag
volatile unsigned char * S2buttonDebounce = &BAKMEM2_H;       // S2 button debounce flag

// TimerA0 UpMode Configuration Parameter
Timer_A_initUpModeParam initUpParam_A0 =
{
        TIMER_A_CLOCKSOURCE_SMCLK,              // SMCLK Clock Source
        TIMER_A_CLOCKSOURCE_DIVIDER_1,          // SMCLK/1 = 2MHz
        30000,                                  // 15ms debounce period
        TIMER_A_TAIE_INTERRUPT_DISABLE,         // Disable Timer interrupt
        TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE ,    // Enable CCR0 interrupt
        TIMER_A_DO_CLEAR,                       // Clear value
        true                                    // Start Timer
};

// TimerA1 ContMode Configuration Parameter
Timer_A_initContinuousModeParam initContParam_A1 =
{
        TIMER_A_CLOCKSOURCE_SMCLK,              // SMCLK Clock Source
        TIMER_A_CLOCKSOURCE_DIVIDER_1,          // SMCLK/1 = 2MHz
        TIMER_A_TAIE_INTERRUPT_ENABLE,
        TIMER_A_DO_CLEAR,                       // Clear value
        true                                    // Start Timer
};


void initialize_motor_driver() {
    // P5.2
    GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN2);
}

int is_daylight() {
    // P2.7
    return GPIO_getInputPinValue(GPIO_PORT_P2, GPIO_PIN7);
}

void turn_on_motor(int index) {
    switch(index) {
    case 0:
        // Irrigation motor in zone B
        if (is_daylight() || !motors_enable[1]) return;
        GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN3);  // P1.3
        motors_ON[0] = 1;
        break;
    case 1:
        // Irrigation motor in zone A
        if (is_daylight() || !motors_enable[0]) return;
        GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN3);  // P5.3
        motors_ON[1] = 1;
        break;
    case 2:
        // Ventilation motor in zone B
        if (!is_daylight() || !motors_enable[1]) return;
        GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN5);  // P1.5
        motors_ON[2] = 1;
        break;
    case 3:
        // Ventilation motor in zone A
        if (!is_daylight() || !motors_enable[0]) return;
        GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN4);  // P1.4
        motors_ON[3] = 1;
        break;
    default:
        break;
    }
}

void turn_off_motor(int index) {
    switch(index) {
    case 0:
        // Irrigation motor in zone B
        GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN3);  // P1.3
        motors_ON[0] = 0;
        break;
    case 1:
        // Irrigation motor in zone A
        GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN3);  // P5.3
        motors_ON[1] = 0;
        break;
    case 2:
        // Ventilation motor in zone B
        GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN5);  // P1.5
        motors_ON[2] = 0;
        break;
    case 3:
        // Ventilation motor in zone A
        GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN4);  // P1.4
        motors_ON[3] = 0;
        break;
    default:
        break;
    }
}

void display() {
    if (display_zone == A) {
        if (display_reading == TEMP)
        {
            display_temp(ADC_readings[3], 1, A);
        }
        else
        {
            display_mois(ADC_readings[1], 4, A);
        }
    }
    else {
        if (display_reading == TEMP)
        {
            display_temp(ADC_readings[2], 1, B);
        }
        else
        {
            display_mois(ADC_readings[0], 4, B);
        }
    }
}

void sendStrOverUart( char* strToSend, int len )
{
    unsigned int i = 0;
    for( i = 0; i < len; i++ )
    {
        while( !(UCA0IFG & UCTXIFG) ) {}
        __delay_cycles(5000);
        uint8_t data = strToSend[i];
        EUSCI_A_UART_transmitData(EUSCI_A0_BASE, data );
    }
    __delay_cycles(5000);
    EUSCI_A_UART_transmitData(EUSCI_A0_BASE, '\r' );
    __delay_cycles(5000);
    EUSCI_A_UART_transmitData(EUSCI_A0_BASE, '\n' );
}

void main(void)
{
    /*
     * Functions with two underscores in front are called compiler intrinsics.
     * They are documented in the compiler user guide, not the IDE or MCU guides.
     * They are a shortcut to insert some assembly code that is not really
     * expressible in plain C/C++. Google "MSP430 Optimizing C/C++ Compiler
     * v18.12.0.LTS" and search for the word "intrinsic" if you want to know
     * more.
     * */

    //Turn off interrupts during initialization
    __disable_interrupt();

    //Stop watchdog timer unless you plan on using it
    WDT_A_hold(WDT_A_BASE);

    // initialize global variables
    *S1buttonDebounce = *S2buttonDebounce = 0;
    ADC_input_channel = 6;
    counters[0] = counters[1] = counters[2] = counters[3] = 0;
    motors_ON[0] = motors_ON[1] = motors_ON[2] = motors_ON[3] = 0;
    ADCState = 0;
    ADC_history_timer = 0;

    // Initializations - see functions for more detail
    Init_GPIO();    //Sets all pins to output low as a default
    Init_PWM();     //Sets up a PWM output
    Init_ADC();     //Sets up the ADC to sample
    Init_Clock();   //Sets up the necessary system clocks
    Init_UART();    //Sets up an echo over a COM port
    Init_LCD();     //Sets up the LaunchPad LCD display

     /*
     * The MSP430 MCUs have a variety of low power modes. They can be almost
     * completely off and turn back on only when an interrupt occurs. You can
     * look up the power modes in the Family User Guide under the Power Management
     * Module (PMM) section. You can see the available API calls in the DriverLib
     * user guide, or see "pmm.h" in the driverlib directory. Unless you
     * purposefully want to play with the power modes, just leave this command in.
     */
    PMM_unlockLPM5(); //Disable the GPIO power-on default high-impedance mode to activate previously configured port settings

    // enable motor driver
    initialize_motor_driver();

    // set threshold values and enable motors for testing
    set_threshold(28, A, TEMP); // 28 degree
    set_threshold(30, B, TEMP); // 30 degree
    set_threshold(20, A, MOIS); // 20 percent
    set_threshold(50, B, MOIS); // 50 percent
    enable_motors(A);
    enable_motors(B);

    //All done initializations - turn interrupts back on.
    __enable_interrupt();

   displayScrollText("GREENHOUSE MONITORING SYSTEM");

    // initialize display channel and start TimerA1
    display_zone = A;
    display_reading = TEMP;
    Timer_A_initContinuousMode(TIMER_A1_BASE, &initContParam_A1);
    // wait for the first round of ADC reading before displaying
    __delay_cycles(300000);
    while(1)
    {
        if( command_ready )
        {
            command_ready = false;

            sendStrOverUart(uart_arr, command_len);
//            __delay_cycles(10000);

            char helpStr1[] =  "Controls:";
            char helpStr2[] =  "help - show this menu";
            char helpStr3[] =  "me <zone> - motor_enable zone={a,b}";
            char helpStr4[] =  "md <zone> - motor_disable zone={a,b}";
            char helpStr5[] =  "st <type> <zone> <threshold value> - set_threshold type={mois, temp}, zone={a,b} threshold_value={valid integer}";
            char helpStr6[] =  "d <type> <zone> - display type={mois, temp}, zone={a,b}";
            char responseBuf[256] = {0};
            char command[10] = {0};
            char motor_type[10];
            char zone = 0;
            int threshold = -1;

            if( StartsWith(uart_arr, "me") )
            {
                char meResponseStr[] = "Motor enable returned status: %s";
                int idx = -1;
                //motor enable
                sscanf( uart_arr, "%s %c", command, &zone );
                if( ( zone == 'a' ) || ( zone == 'A' ) )
                {
                    enable_motors(A);
                    sprintf( responseBuf, meResponseStr, "SUCCESS" );
                    sendStrOverUart(responseBuf, strlen( responseBuf ) );
                }
                else if( ( zone == 'b' ) || ( zone == 'B' ) )
                {
                    enable_motors(B);
                    sprintf( responseBuf, meResponseStr, "SUCCESS" );
                    sendStrOverUart(responseBuf, strlen( responseBuf ) );
                }
                else
                {
                    sprintf( responseBuf, meResponseStr, "ERROR" );
                    sendStrOverUart(responseBuf, strlen( responseBuf ) );
                }
            }
            else if( StartsWith(uart_arr, "md") )
            {
                char meResponseStr[] = "Motor disable returned status: %s";
                int idx = -1;
                //motor disable
                sscanf( uart_arr, "%s %c", command, &zone );
                if( ( zone == 'a' ) || ( zone == 'A' ) )
                {
                    disable_motors(A);
                    sprintf( responseBuf, meResponseStr, "SUCCESS" );
                    sendStrOverUart(responseBuf, strlen( responseBuf ) );
                }
                else if( ( zone == 'b' ) || ( zone == 'B' ) )
                {
                    disable_motors(B);
                    sprintf( responseBuf, meResponseStr, "SUCCESS" );
                    sendStrOverUart(responseBuf, strlen( responseBuf ) );
                }
                else {
                    sprintf( responseBuf, meResponseStr, "ERROR" );
                    sendStrOverUart(responseBuf, strlen( responseBuf ) );
                }
            }
            else if( StartsWith(uart_arr, "help") )
            {
                sendStrOverUart(helpStr1, strlen( helpStr1 ) );
                sendStrOverUart(helpStr2, strlen( helpStr2 ) );
                sendStrOverUart(helpStr3, strlen( helpStr3 ) );
                sendStrOverUart(helpStr4, strlen( helpStr4 ) );
                sendStrOverUart(helpStr5, strlen( helpStr5 ) );
                sendStrOverUart(helpStr6, strlen( helpStr6 ) );
                sendStrOverUart(' ', 1);
            }
            else if( StartsWith(uart_arr, "st") )
            {
                char meResponseStr[] = "Set threshold value returned status: %s";
                bool success = true;
                ZONE newZone;
                READING_TYPE newType;
                //set threshold
                sscanf( uart_arr, "%s %s %c %d", command, motor_type, &zone, &threshold );
                if( ( zone == 'a' ) || ( zone == 'A' ) )
                {
                    newZone = A;
                }
                else if( ( zone == 'b' ) || ( zone == 'B' ) )
                {
                    newZone = B;
                }
                else
                {
                    success = false;
                }

                if( 0 == strcmp(motor_type, "mois") )
                {
                    newType = MOIS;
                }
                else if( 0 == strcmp(motor_type, "temp") )
                {
                    newType = TEMP;
                }
                else
                {
                    success = false;
                }
                if( threshold < 0 )
                {
                    success = false;
                }

                if( success == true )
                {
                    int status = set_threshold(threshold, newZone, newType);
                    if (status == -1) success = false;
                }

                if ( success == true )
                {
                    sprintf( responseBuf, meResponseStr, "SUCCESS" );
                    sendStrOverUart(responseBuf, strlen( responseBuf ) );
                }
                else
                {
                    sprintf( responseBuf, meResponseStr, "ERROR" );
                    sendStrOverUart(responseBuf, strlen( responseBuf ) );
                }
            }
            else if( StartsWith(uart_arr, "d") )
            {
                char dResponseStr[] = "Display returned status: %s";
                bool success = true;
                ZONE newZone;
                READING_TYPE newType;
                sscanf( uart_arr, "%s %s %c", command, motor_type, &zone );
                if( ( zone == 'a' ) || ( zone == 'A' ) )
                {
                    newZone = A;
                }
                else if( ( zone == 'b' ) || ( zone == 'B' ) )
                {
                    newZone = B;
                }
                else
                {
                    success = false;
                }

                if( 0 == strcmp(motor_type, "mois") )
                {
                    newType = MOIS;
                }
                else if( 0 == strcmp(motor_type, "temp") )
                {
                    sendStrOverUart("here", 4);
                    newType = TEMP;
                }
                else
                {
                    success = false;
                }

                if( success )
                {
                    display_zone = newZone;
                    display_reading = newType;
                    sprintf( responseBuf, dResponseStr, "SUCCESS" );
                    sendStrOverUart(responseBuf, strlen( responseBuf ) );
                }
                else
                {
                    sprintf( responseBuf, dResponseStr, "ERROR" );
                    sendStrOverUart(responseBuf, strlen( responseBuf ) );
                }
            }
//            printf( "command %s\nzone: %c\nthreshold: %d\n", command, zone, threshold );
            command_len = 0;
            memset( uart_arr, 0, UART_BUFF_SIZE );
        }
        display();
        __delay_cycles(200000);
    }

    /*
     * You can use the following code if you plan on only using interrupts
     * to handle all your system events since you don't need any infinite loop of code.
     *
     * //Enter LPM0 - interrupts only
     * __bis_SR_register(LPM0_bits);
     * //For debugger to let it know that you meant for there to be no more code
     * __no_operation();
    */

}

#pragma vector=USCI_A0_VECTOR
__interrupt
void EUSCIA0_ISR(void)
{
    uint8_t RxStatus = EUSCI_A_UART_getInterruptStatus(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG);
    EUSCI_A_UART_clearInterrupt(EUSCI_A0_BASE, RxStatus);
    if (RxStatus)
    {
        uint8_t uart_data = EUSCI_A_UART_receiveData(EUSCI_A0_BASE);
        // newline or carriage return
        if( ( uart_data == 10 ) || ( uart_data == 13 ) || ( uart_arr_idx >= UART_BUFF_SIZE ) )
        {
            command_ready = true;

            command_len = uart_arr_idx;
            //reset arr idx
            uart_arr_idx = 0;
        }
        // backspace
        else if( uart_data == 8 )
        {
            if( uart_arr_idx > 0 )
            {
                uart_arr_idx--;
            }
        }
        else
        {
            uart_arr[uart_arr_idx] = uart_data;
            uart_arr_idx++;
        }
    }
}


void Init_GPIO(void)
{
    // Set all GPIO pins to output low to prevent floating input and reduce power consumption
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P6, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P7, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);

    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P4, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P6, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P7, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P8, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);

    //Set LaunchPad switches as inputs - they are active low, meaning '1' until pressed
    GPIO_setAsInputPinWithPullUpResistor(SW1_PORT, SW1_PIN);
    GPIO_setAsInputPinWithPullUpResistor(SW2_PORT, SW2_PIN);
    // Configure button S1 (P1.2) interrupt
    GPIO_selectInterruptEdge(GPIO_PORT_P1, GPIO_PIN2, GPIO_HIGH_TO_LOW_TRANSITION);
    GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P1, GPIO_PIN2);
    GPIO_clearInterrupt(GPIO_PORT_P1, GPIO_PIN2);
    GPIO_enableInterrupt(GPIO_PORT_P1, GPIO_PIN2);

    // Configure button S2 (P2.6) interrupt
    GPIO_selectInterruptEdge(GPIO_PORT_P2, GPIO_PIN6, GPIO_HIGH_TO_LOW_TRANSITION);
    GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P2, GPIO_PIN6);
    GPIO_clearInterrupt(GPIO_PORT_P2, GPIO_PIN6);
    GPIO_enableInterrupt(GPIO_PORT_P2, GPIO_PIN6);

    // Configure light sensor (P2.7) as input pin
    GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P2, GPIO_PIN7);

    // Disable the GPIO power-on default high-impedance mode
    // to activate previously configured port settings
    PMM_unlockLPM5();
}

/* Clock System Initialization */
void Init_Clock(void)
{
    /*
     * The MSP430 has a number of different on-chip clocks. You can read about it in
     * the section of the Family User Guide regarding the Clock System ('cs.h' in the
     * driverlib).
     */

    /*
     * On the LaunchPad, there is a 32.768 kHz crystal oscillator used as a
     * Real Time Clock (RTC). It is a quartz crystal connected to a circuit that
     * resonates it. Since the frequency is a power of two, you can use the signal
     * to drive a counter, and you know that the bits represent binary fractions
     * of one second. You can then have the RTC module throw an interrupt based
     * on a 'real time'. E.g., you could have your system sleep until every
     * 100 ms when it wakes up and checks the status of a sensor. Or, you could
     * sample the ADC once per second.
     */
    //Set P4.1 and P4.2 as Primary Module Function Input, XT_LF
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P4, GPIO_PIN1 + GPIO_PIN2, GPIO_PRIMARY_MODULE_FUNCTION);

    // Set external clock frequency to 32.768 KHz
    CS_setExternalClockSource(32768);
    // Set ACLK = XT1
    CS_initClockSignal(CS_ACLK, CS_XT1CLK_SELECT, CS_CLOCK_DIVIDER_1);
    // Initializes the XT1 crystal oscillator
    CS_turnOnXT1LF(CS_XT1_DRIVE_1);
    // Set SMCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_SMCLK, CS_DCOCLKDIV_SELECT, CS_CLOCK_DIVIDER_1);
    // Set MCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_MCLK, CS_DCOCLKDIV_SELECT, CS_CLOCK_DIVIDER_1);
}

/* UART Initialization */
void Init_UART(void)
{
    /* UART: It configures P1.0 and P1.1 to be connected internally to the
     * eSCSI module, which is a serial communications module, and places it
     * in UART mode. This let's you communicate with the PC via a software
     * COM port over the USB cable. You can use a console program, like PuTTY,
     * to type to your LaunchPad. The code in this sample just echos back
     * whatever character was received.
     */

    //Configure UART pins, which maps them to a COM port over the USB cable
    //Set P1.0 and P1.1 as Secondary Module Function Input.
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_PRIMARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN0, GPIO_PRIMARY_MODULE_FUNCTION);

    /*
     * UART Configuration Parameter. These are the configuration parameters to
     * make the eUSCI A UART module to operate with a 9600 baud rate. These
     * values were calculated using the online calculator that TI provides at:
     * http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP430BaudRateConverter/index.html
     */

    //SMCLK = 1MHz, Baudrate = 9600
    //UCBRx = 6, UCBRFx = 8, UCBRSx = 17, UCOS16 = 1
    EUSCI_A_UART_initParam param = {0};
        param.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
        param.clockPrescalar    = 6;
        param.firstModReg       = 8;
        param.secondModReg      = 17;
        param.parity            = EUSCI_A_UART_NO_PARITY;
        param.msborLsbFirst     = EUSCI_A_UART_LSB_FIRST;
        param.numberofStopBits  = EUSCI_A_UART_ONE_STOP_BIT;
        param.uartMode          = EUSCI_A_UART_MODE;
        param.overSampling      = 1;

    if(STATUS_FAIL == EUSCI_A_UART_init(EUSCI_A0_BASE, &param))
    {
        return;
    }

    EUSCI_A_UART_enable(EUSCI_A0_BASE);

    EUSCI_A_UART_clearInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);

    // Enable EUSCI_A0 RX interrupt
    EUSCI_A_UART_enableInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);
}

/* PWM Initialization */
void Init_PWM(void)
{
    /*
     * The internal timers (TIMER_A) can auto-generate a PWM signal without needing to
     * flip an output bit every cycle in software. The catch is that it limits which
     * pins you can use to output the signal, whereas manually flipping an output bit
     * means it can be on any GPIO. This function populates a data structure that tells
     * the API to use the timer as a hardware-generated PWM source.
     *
     */
    //Generate PWM - Timer runs in Up-Down mode
    param.clockSource           = TIMER_A_CLOCKSOURCE_SMCLK;
    param.clockSourceDivider    = TIMER_A_CLOCKSOURCE_DIVIDER_1;
    param.timerPeriod           = TIMER_A_PERIOD; //Defined in main.h
    param.compareRegister       = TIMER_A_CAPTURECOMPARE_REGISTER_1;
    param.compareOutputMode     = TIMER_A_OUTPUTMODE_RESET_SET;
    param.dutyCycle             = HIGH_COUNT; //Defined in main.h

    //PWM_PORT PWM_PIN (defined in main.h) as PWM output
    GPIO_setAsPeripheralModuleFunctionOutputPin(PWM_PORT, PWM_PIN, GPIO_PRIMARY_MODULE_FUNCTION);
}

void Init_ADC(void)
{
    /*
     * To use the ADC, you need to tell a physical pin to be an analog input instead
     * of a GPIO, then you need to tell the ADC to use that analog input. Defined
     * these in main.h for A9 on P8.1.
     */

    //Set ADC_IN to input direction
    GPIO_setAsPeripheralModuleFunctionInputPin(ADC_IN_PORT, ADC_IN_PIN, GPIO_PRIMARY_MODULE_FUNCTION);

    //Initialize the ADC Module
    /*
     * Base Address for the ADC Module
     * Use internal ADC bit as sample/hold signal to start conversion
     * USE MODOSC 5MHZ Digital Oscillator as clock source
     * Use default clock divider of 1
     */
    ADC_init(ADC_BASE,
             ADC_SAMPLEHOLDSOURCE_SC,
             ADC_CLOCKSOURCE_ADCOSC,
             ADC_CLOCKDIVIDER_1);

    ADC_enable(ADC_BASE);

    /*
     * Base Address for the ADC Module
     * Sample/hold for 16 clock cycles
     * Do not enable Multiple Sampling
     */
    ADC_setupSamplingTimer(ADC_BASE,
                           ADC_CYCLEHOLD_16_CYCLES,
                           ADC_MULTIPLESAMPLESDISABLE);


    ADC_clearInterrupt(ADC_BASE,
                       ADC_COMPLETED_INTERRUPT | ADC_ABOVETHRESHOLD_INTERRUPT | ADC_BELOWTHRESHOLD_INTERRUPT | ADC_INSIDEWINDOW_INTERRUPT);

    //Enable Memory Buffer interrupt
    ADC_enableInterrupt(ADC_BASE,
                        ADC_COMPLETED_INTERRUPT | ADC_ABOVETHRESHOLD_INTERRUPT | ADC_BELOWTHRESHOLD_INTERRUPT| ADC_INSIDEWINDOW_INTERRUPT);

    PMM_enableInternalReference(); // enable internal 1.5V reference
}

//ADC interrupt service routine
#pragma vector=ADC_VECTOR
__interrupt
void ADC_ISR(void)
{
    switch(__even_in_range(ADCIV, ADCIV_ADCIFG))
    {
        case ADCIV_NONE : break;
        case ADCIV_ADCOVIFG : break;
        case ADCIV_ADCTOVIFG: break;
        case ADCIV_ADCHIIFG: break;
        case ADCIV_ADCLOIFG: break;
        case ADCIV_ADCINIFG: break;
        case ADCIV_ADCIFG:
            ADCState = 0; //Not busy anymore
            ADCResult = ADC_getResults(ADC_BASE);
            ADC_clearInterrupt(ADC_BASE, ADC_COMPLETED_INTERRUPT);
            break;
    }
}


/*
 * PORT1 Interrupt Service Routine
 * Handles S1 button press interrupt
 * Function: switch display zone
 */
#pragma vector = PORT1_VECTOR
__interrupt void PORT1_ISR(void)
{
    switch(__even_in_range(P1IV, P1IV_P1IFG7))
    {
        case P1IV_NONE : break;
        case P1IV_P1IFG0 : break;
        case P1IV_P1IFG1 : break;
        case P1IV_P1IFG2 :
            if ((*S1buttonDebounce) != 0) break;
            *S1buttonDebounce = 1;
            // switch display zone
            display_zone = (display_zone == A)? B : A;
            // Start debounce timer
            Timer_A_initUpMode(TIMER_A0_BASE, &initUpParam_A0);
            break;
        case P1IV_P1IFG3 : break;
        case P1IV_P1IFG4 : break;
        case P1IV_P1IFG5 : break;
        case P1IV_P1IFG6 : break;
        case P1IV_P1IFG7 : break;
    }
}


/*
 * PORT2 Interrupt Service Routine
 * Handles S2 button press interrupt
 * Function: switch display temp/moisture reading
 */
#pragma vector = PORT2_VECTOR
__interrupt void PORT2_ISR(void)
{
    switch(__even_in_range(P2IV, P2IV_P2IFG7))
    {
        case P2IV_NONE : break;
        case P2IV_P2IFG0 : break;
        case P2IV_P2IFG1 : break;
        case P2IV_P2IFG2 : break;
        case P2IV_P2IFG3 : break;
        case P2IV_P2IFG4 : break;
        case P2IV_P2IFG5 : break;
        case P2IV_P2IFG6 :
            if ((*S2buttonDebounce) != 0) break;
            *S2buttonDebounce = 1;
            // switch display reading type
            display_reading = (display_reading == TEMP) ? MOIS : TEMP;
            // Start debounce timer
            Timer_A_initUpMode(TIMER_A0_BASE, &initUpParam_A0);
            break;
        case P2IV_P2IFG7 : break;
    }
}


/*
 * Timer A0 Interrupt Service Routine
 * Used as button debounce timer
 */
#pragma vector = TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR (void)
{
    // Button S1 released
    if (P1IN & BIT2)
    {
        *S1buttonDebounce = 0;                                   // Clear button debounce
    }

    // Button S2 released
    if (P2IN & BIT6)
    {
        *S2buttonDebounce = 0;                                   // Clear button debounce
    }
}

#pragma vector = TIMER1_A1_VECTOR
__interrupt void TIMER1_A1_ISR (void)
{
    if (ADCState == 0)
    {
            int index = ADC_input_channel - 6;
            ADC_readings[index] = ADCResult;
            // if the value is temperature reading
            if (index == 3 || index == 2) {
                if (!motors_ON[index] && ADCResult > thresholds[index]) {
                    counters[index]++;
                    if (counters[index] > barrier) {
                        turn_on_motor(index);
                        counters[index] = 0;
                    }
                }
                else if (motors_ON[index] && ADCResult <= thresholds[index]) {
                    counters[index]++;
                    if (counters[index] > barrier) {
                        turn_off_motor(index);
                        counters[index] = 0;
                    }
                }
            }
            // else if the value is moisture reading
            else {
                if (!motors_ON[index] && ADCResult < thresholds[index]) {
                    counters[index]++;
                    if (counters[index] > barrier) {
                        turn_on_motor(index);
                        counters[index] = 0;
                    }
                }
                else if (motors_ON[index] && ADCResult >= thresholds[index]) {
                    counters[index]++;
                    if (counters[index] > barrier) {
                        turn_off_motor(index);
                        counters[index] = 0;
                    }
                }
            }

            // switch ADC input channel
            index = (index + 1) % 4;
            ADC_input_channel = index + 6;
            ADC_disableConversions(ADC_BASE, ADC_COMPLETECONVERSION);
            ADC_configureMemory(
                ADC_BASE,
                ADC_input_channel, // ADC input channel
                ADC_VREFPOS_INT,   // use internal 1.5V reference voltage
                ADC_VREFNEG_AVSS
            );
            ADCState = 1; // Set flag to indicate ADC is busy - ADC ISR (interrupt) will clear it
            ADC_startConversion(ADC_BASE, ADC_SINGLECHANNEL); // start conversion
    }

    // check daylight status
    if (is_daylight()) {
        // turn of irrigation motors during daylight if any is on
        if (motors_ON[0])
            turn_off_motor(0);
        else if (motors_ON[1])
            turn_off_motor(1);
    } else {
        // turn of ventilation motors during night if any is on
        if (motors_ON[2])
            turn_off_motor(2);
        else if (motors_ON[3])
            turn_off_motor(3);
    }

    ADC_history_timer++;
    if (ADC_history_timer > ADC_HISTORY_TIMER_CYCLE) { // about 5s reset the counter
        counters[0] = counters[1] = counters[2] = counters[3] = 0; // reset history counters
        ADC_history_timer = 0;
        // P4.0 led indicator for timer
        GPIO_toggleOutputOnPin(GPIO_PORT_P4, GPIO_PIN0);
    }
    Timer_A_clearTimerInterrupt(TIMER_A1_BASE);
}

